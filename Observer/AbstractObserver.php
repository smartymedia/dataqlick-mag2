<?php
namespace Smartymedia\DataQlick\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\HTTP\ZendClientFactory;

abstract class AbstractObserver implements ObserverInterface
{
    const REG_KEY = '';
    const WEBHOOK_URL = 'https://dataqlick.com/webhook/plugin/';
    const WEBHOOK_TYPE = '';

    /** @var \Magento\Framework\Logger\Monolog */
    protected $logger;
    protected $registry;
    protected $httpClientFactory;
    protected $model;
    protected $dq;


    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\Registry $registry,
        ZendClientFactory $httpClientFactory,
        \Smartymedia\DataQlick\Model\DQApi $dq
    )
    {
        $this->logger = $loggerInterface;
        $this->registry = $registry;
        $this->httpClientFactory = $httpClientFactory;
        $this->dq = $dq;
    }


    protected function updateEntity($entityId) {
        $this->logger->debug('Observer: saving entity '.$entityId.' to registry '.static::REG_KEY);

        $entityIdList = $this->registry->registry(static::REG_KEY);
        if($entityIdList) { // key exists
            $entityIdList[] = $entityId;
            $entityIdList = array_unique($entityIdList);
            $this->registry->unregister(static::REG_KEY);
            $this->registry->register(static::REG_KEY, $entityIdList);
        } else {
            $this->registry->register(static::REG_KEY, [$entityId]);
            register_shutdown_function(array($this, 'callWebhook'));
        }
    }

    public function callWebhook() {

        try {
            $this->dq->getAuthToken();

            if(!$this->dq->isAuthorized()) return;

            $entityIdList = $this->registry->registry(static::REG_KEY);
            $this->logger->debug('Observer: shutdown, registry '.static::REG_KEY.' values '.json_encode($entityIdList));
            if($entityIdList) { // key exists
                $this->model->searchCriteria->setPageSize(count($entityIdList));
                $this->model->searchCriteria->setCurrentPage(1);

                $this->model->setFilters([$this->model->filterGroup
                        ->setFilters([
                            $this->model->filterBuilder
                                ->setField('entity_id')
                                ->setConditionType('in')
                                ->setValue($entityIdList)
                                ->create()
                        ])
                        ->create()
                    ]);


                $itemsResult = $this->model->getItems();
                $jsonData = json_encode($itemsResult, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

                $webHookUrl = static::WEBHOOK_URL.$this->dq->getCustomerId().'/'.static::WEBHOOK_TYPE.'/update';

                $client = $this->httpClientFactory->create();
                $client->setUri($webHookUrl);
                $client->setConfig(['timeout' => 10]);
                $client->setMethod(\Zend_Http_Client::POST);
                $client->setRawData($jsonData, 'application/json');
                $this->logger->debug('Observer: sending data to '.$webHookUrl);

                $response = $client->request();
                $this->logger->debug('Observer: response '.$response->getBody());
            }
        } catch (\Exception $e) {
        }
    }

}