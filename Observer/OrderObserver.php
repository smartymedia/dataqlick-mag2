<?php
namespace Smartymedia\DataQlick\Observer;

use Smartymedia\DataQlick\Observer\AbstractObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\HTTP\ZendClientFactory;

class OrderObserver extends AbstractObserver
{
    const REG_KEY = 'Smartymedia_DataQlick_updated_orders';
    const WEBHOOK_TYPE = 'orders';

    protected $dq;

    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\Registry $registry,
        ZendClientFactory $httpClientFactory,
        \Smartymedia\DataQlick\Model\DQApi $dq,
        \Smartymedia\DataQlick\Model\Sales $sales
    )
    {
        parent::__construct($loggerInterface, $registry, $httpClientFactory, $dq);
        $this->model = $sales;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
            if (!($order instanceof \Magento\Framework\Model\AbstractModel)) {
            try {
                if($observer->getEvent()->getTrack() instanceof \Magento\Sales\Api\Data\ShipmentTrackInterface) {
                    $order = $observer->getEvent()->getTrack()->getShipment()->getOrder();
                }
            } catch (\Exception $e) {
            }
        }
        if (!($order instanceof \Magento\Framework\Model\AbstractModel)) {
            try {
                if($observer->getEvent()->getInvoice() instanceof \Magento\Sales\Api\Data\InvoiceInterface) {
                    $order = $observer->getEvent()->getInvoice()->getOrder();
                }
            } catch (\Exception $e) {
            }
        }
        if (!($order instanceof \Magento\Framework\Model\AbstractModel)) {
            try {
                if($observer->getEvent()->getCreditmemo() instanceof \Magento\Sales\Api\Data\CreditmemoInterface) {
                    $order = $observer->getEvent()->getCreditmemo()->getOrder();
                }
            } catch (\Exception $e) {
            }
        }


        if ($order instanceof \Magento\Framework\Model\AbstractModel) {


            $this->updateEntity($order->getId());
            $this->logger->debug('OrderObserver '.$order->getId());
        } else {
            $this->logger->debug('OrderObserver - no order');
        }
    }

}