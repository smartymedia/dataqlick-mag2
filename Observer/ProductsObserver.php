<?php
namespace Smartymedia\DataQlick\Observer;

use Smartymedia\DataQlick\Observer\AbstractObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\HTTP\ZendClientFactory;

class ProductsObserver extends AbstractObserver
{
    const REG_KEY = 'Smartymedia_DataQlick_updated_products';
    const WEBHOOK_TYPE = 'products';


    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\Registry $registry,
        ZendClientFactory $httpClientFactory,
        \Smartymedia\DataQlick\Model\DQApi $dq,
        \Smartymedia\DataQlick\Model\Products $products
    )
    {
        parent::__construct($loggerInterface, $registry, $httpClientFactory, $dq);
        $this->model = $products;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getProduct();

        if ($product instanceof \Magento\Framework\Model\AbstractModel) {


            $this->updateEntity($product->getId());

            $this->logger->debug('ProductsObserver '.$product->getId());
        } else {
            $this->logger->debug('ProductsObserver - no product');
        }
    }

}