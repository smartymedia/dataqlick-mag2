<?php

namespace Smartymedia\DataQlick\Controller\Ajax;

use Magento\Framework\Controller\ResultFactory;

class Shop extends \Smartymedia\DataQlick\Controller\AbstractController
{
    protected $dq;
    protected $storeManager;
    protected $paymentHelper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Smartymedia\DataQlick\Model\DQApi $dq,
        \Magento\Payment\Helper\Data $paymentHelper
    )
    {
        $this->dq = $dq;
        $this->storeManager = $storeManager;
        $this->paymentHelper = $paymentHelper;
        parent::__construct($context);
    }


    public function execute()
    {
        if(!$this->verifyToken()) return $this->accessDenied();

        $methodList = $this->paymentHelper->getPaymentMethodList();

        foreach($methodList as $code => &$label) {
            if(empty($label)) $label = $code;
        }

        $result = [
            'name' => $this->storeManager->getStore()->getName(),
            'currency' => $this->storeManager->getStore()->getCurrentCurrency()->getCode(),
            'time' => date('c'),
            'methods' => $methodList,
        ];

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setJsonData(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        return $resultJson;
    }
}