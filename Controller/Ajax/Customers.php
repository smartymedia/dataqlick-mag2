<?php

namespace Smartymedia\DataQlick\Controller\Ajax;

use Magento\Framework\Controller\ResultFactory;

class Customers extends \Smartymedia\DataQlick\Controller\AbstractController
{
    protected $customers;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Smartymedia\DataQlick\Model\Customers $customers
    )
    {
        $this->customers = $customers;
        parent::__construct($context);
    }


    public function execute()
    {
        if(!$this->verifyToken()) return $this->accessDenied();

        $page = (int)$this->getRequest()->getParam('page', 1);
        $pagesize = (int)$this->getRequest()->getParam('pagesize', 10);

        $this->customers->searchCriteria->setPageSize($pagesize);
        $this->customers->searchCriteria->setCurrentPage($page);

        $result = $this->customers->getItems();

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setJsonData(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        return $resultJson;
    }
}