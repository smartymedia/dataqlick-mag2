<?php

namespace Smartymedia\DataQlick\Controller\Ajax;

use Magento\Framework\Controller\ResultFactory;

class Products extends \Smartymedia\DataQlick\Controller\AbstractController
{
    protected $products;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Smartymedia\DataQlick\Model\Products $products
    )
    {
        $this->products = $products;
        parent::__construct($context);
    }


    public function execute()
    {
        if(!$this->verifyToken()) return $this->accessDenied();

        $request = $this->getRequest();
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if($request->isPost()) {
            try {
                $postdata = json_decode(file_get_contents("php://input"), true);
                if(!is_array($postdata)) throw new \Exception(__('Bad request'));
                foreach($postdata as $data)
                    $this->products->update($data);
                $result = ['status' => 'ok'];
            } catch (\Exception $e) {
                $result = ['error' => $e->getMessage()];
            }
            $resultJson->setJsonData(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            return $resultJson;
        } elseif($request->isGet()) {
            try {
                $page = (int)$request->getParam('page', 1);
                $pagesize = (int)$request->getParam('pagesize', 10);
                $updatedAfter = $request->getParam('updatedAfter', null);
                $entityIds = $request->getParam('productid', null) ? explode(',', $request->getParam('productid', null)) : array();

                if($updatedAfter) {
                    $updatedAfter = strtotime($updatedAfter);
                    if(!$updatedAfter) throw new \Exception(__('Bad datetime format in updatedAfter'));
                    $filters = $this->products->getFilters();
                    $filters[] = $this->products->filterGroup
                        ->setFilters([
                            $this->products->filterBuilder
                                ->setField('updated_at')
                                ->setConditionType('gt')
                                ->setValue(date('Y-m-d H:i:s', $updatedAfter))
                                ->create()
                        ])
                        ->create();
                    $this->products->setFilters($filters);
                }

                if(count($entityIds) > 0) {
                    $filters = $this->products->getFilters();
                    $filters[] = $this->products->filterGroup
                        ->setFilters([
                            $this->products->filterBuilder
                                ->setField('entity_id')
                                ->setConditionType('in')
                                ->setValue($entityIds)
                                ->create()
                        ])
                        ->create();
                    $this->products->setFilters($filters);
                }

                $this->products->searchCriteria->setPageSize($pagesize);
                $this->products->searchCriteria->setCurrentPage($page);

                $result = $this->products->getItems();

            } catch (\Exception $e) {
                $result = ['error' => $e->getMessage()];
                $resultJson->setHttpResponseCode(500);
            }
            $resultJson->setJsonData(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            return $resultJson;
        }
    }
}