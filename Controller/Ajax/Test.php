<?php

namespace Smartymedia\DataQlick\Controller\Ajax;

use Magento\Framework\Controller\ResultFactory;

class Test extends \Smartymedia\DataQlick\Controller\AbstractController
{
    protected $sales;
    protected $dq;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Smartymedia\DataQlick\Model\Sales $sales,
        \Smartymedia\DataQlick\Model\DQApi $dq
    )
    {
        $this->sales = $sales;
        $this->dq = $dq;
        parent::__construct($context);
    }


    public function execute()
    {
        if(!$this->verifyToken()) return $this->accessDenied();

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $request = $this->getRequest();

        $this->dq->getAuthToken();

        $access_token = $this->dq->getShopToken();

        $result = ['token' => $access_token];

        $resultJson->setJsonData(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        return $resultJson;
    }
}