<?php

namespace Smartymedia\DataQlick\Controller\Ajax;

use Magento\Framework\Controller\ResultFactory;

class Sales extends \Smartymedia\DataQlick\Controller\AbstractController
{
    protected $sales;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Smartymedia\DataQlick\Model\Sales $sales
    )
    {
        $this->sales = $sales;
        parent::__construct($context);
    }


    public function execute()
    {
        if(!$this->verifyToken()) return $this->accessDenied();

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        try {
            $request = $this->getRequest();
            $page = (int)$request->getParam('page', 1);
            $pagesize = (int)$request->getParam('pagesize', 10);
            $updatedAfter = $request->getParam('updatedAfter', null);
            $entityIds = $request->getParam('id', null) ? explode(',', $request->getParam('id', null)) : array();

            if($updatedAfter) {
                $updatedAfter = strtotime($updatedAfter);
                if(!$updatedAfter) throw new \Exception(__('Bad datetime format in updatedAfter'));
                $filters = $this->sales->getFilters();
                $filters[] = $this->sales->filterGroup
                    ->setFilters([
                        $this->sales->filterBuilder
                            ->setField('updated_at')
                            ->setConditionType('gt')
                            ->setValue(date('Y-m-d H:i:s', $updatedAfter))
                            ->create()
                    ])
                    ->create();
                $this->sales->setFilters($filters);
            }

            if(count($entityIds) > 0) {
                $filters = $this->sales->getFilters();
                $filters[] = $this->sales->filterGroup
                    ->setFilters([
                        $this->sales->filterBuilder
                            ->setField('entity_id')
                            ->setConditionType('in')
                            ->setValue($entityIds)
                            ->create()
                    ])
                    ->create();
                $this->sales->setFilters($filters);
            }

            $this->sales->searchCriteria->setPageSize($pagesize);
            $this->sales->searchCriteria->setCurrentPage($page);

            $result = $this->sales->getItems();
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage()];
            $resultJson->setHttpResponseCode(500);
        }

        $resultJson->setJsonData(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        return $resultJson;
    }
}