<?php

namespace Smartymedia\DataQlick\Controller;

use Magento\Framework\Controller\ResultFactory;

abstract class AbstractController extends \Magento\Framework\App\Action\Action
{
    public function verifyToken() {
        $request = $this->getRequest();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $dq = $objectManager->create('\Smartymedia\DataQlick\Model\DQApi');

        try {
            $dq->getAuthToken();

            if(!$dq->isAuthorized()) return false;

            $shop_token = $dq->getShopToken();
            if(empty($shop_token)) return false;
            if($request->getParam('token') != $shop_token) return false;
            return true;
        } catch(\Exception $e) {
            return false;
        }
    }

    public function accessDenied() {
        $result = ['error' => 'Access Denied'];
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setJsonData(json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        return $resultJson;
    }
}