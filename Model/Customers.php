<?php

namespace Smartymedia\DataQlick\Model;

class Customers  {
    protected $customerRepository;
    protected $filterGroup;
    protected $address;

    private $filters;

    public $filterBuilder;
    public $searchCriteria;

    public function __construct(
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
        \Magento\Framework\Api\SearchCriteriaInterface $criteria,
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Customer\Model\Data\Address $address
    )
    {
        $this->customerRepository = $customerRepository;
        $this->searchCriteria = $criteria;
        $this->filterGroup = $filterGroup;
        $this->filterBuilder = $filterBuilder;
        $this->address = $address;

        $this->searchCriteria->setPageSize(1);
        $this->searchCriteria->setCurrentPage(1);
    }

    function setFilters($filters) {
        $this->filters = $filters;
    }

    function getFilters() {
        return $this->filters;
    }

    function getItems() {
        try {
            $this->filterGroup->setFilters($this->getFilters());
            $this->searchCriteria->setFilterGroups([$this->filterGroup]);
            $customersList = $this->customerRepository->getList($this->searchCriteria);
            $customersItems = $customersList->getItems();

            $customers = [];
            foreach($customersItems as $customerItem) {
                $shippingAddressId = $customerItem->getDefaultShipping();
                $billingAddressId = $customerItem->getDefaultBilling();
                $addresses = $customerItem->getAddresses();
                $shippingAddress = $billingAddress = $this->address;
                foreach($customerItem->getAddresses() as $address) {
                    if($address->getId() == $shippingAddressId) $shippingAddress = $address;
                    if($address->getId() == $billingAddressId) $billingAddress = $address;
                }
                $customer = [
                    'id' => (int)$customerItem->getId(),
                    'displayName' => $customerItem->getFirstname(). ' ' .$customerItem->getLastname(),
                    'firstName' => $customerItem->getFirstname(),
                    'lastName' => $customerItem->getLastname(),
                    'email' => $customerItem->getEmail(),
                    'shippingAddress' => [
                        'companyName' => $shippingAddress->getCompany() ? $shippingAddress->getCompany() : '',
                        'phone' => $shippingAddress->getTelephone() ? $shippingAddress->getTelephone() : '',
                        'street' => implode("\n", $shippingAddress->getStreet()),
                        'city' => $shippingAddress->getCity() ? $shippingAddress->getCity() : '',
                        'state' => $shippingAddress->getRegion() ? $shippingAddress->getRegion()->getRegionCode() : '',
                        'zip' => $shippingAddress->getPostcode() ? $shippingAddress->getPostcode() : '',
                        'country' => $shippingAddress->getCountryId() ? $shippingAddress->getCountryId() : '',
                        'email' => $customerItem->getEmail(),
                    ],
                    'billingAddress' => [
                        'companyName' => $billingAddress->getCompany() ? $billingAddress->getCompany() : '',
                        'phone' => $billingAddress->getTelephone() ? $billingAddress->getTelephone() : '',
                        'street' => implode("\n", $billingAddress->getStreet()),
                        'city' => $billingAddress->getCity() ? $billingAddress->getCity() : '',
                        'state' => $billingAddress->getRegion() ? $billingAddress->getRegion()->getRegionCode() : '',
                        'zip' => $billingAddress->getPostcode() ? $billingAddress->getPostcode() : '',
                        'country' => $billingAddress->getCountryId() ? $billingAddress->getCountryId() : '',
                        'email' => $customerItem->getEmail(),
                    ]
                ];
                $customers[] = $customer;
            }

            $totalCount = $customersList->getTotalCount();
            $page = $this->searchCriteria->getCurrentPage();
            $pagesize = $this->searchCriteria->getPageSize();
            if($pagesize < 1) {
                return $this->error('WRONG_PAGE', 'Page should be > 0');
            }
            $pagemax = ceil($totalCount / $pagesize);
            if($page > $pagemax) $page = $pagemax;
            if($page < 1) $page = 1;

            $result = [
                'count' => $totalCount,
                'page' => $page,
                'pagesize' => $pagesize,
                'pagemax' => $pagemax,
                'results' => $customers,
            ];
            return $result;
        } catch (\Exception $e) {
            return $this->error('EXCEPTION', $e->getMessage());
        }
    }


    private function error($code, $message) {
        return [
            'error' => $code,
            'message' => $message
        ];
    }

}