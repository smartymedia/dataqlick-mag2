<?php

namespace Smartymedia\DataQlick\Model;

use Magento\Catalog\Model\Product;
use \Magento\Catalog\Model\Product\Attribute\Source\Status;

class Products  {
    protected $productMetadata;
    protected $productRepository;
    protected $productStatus;
    protected $productVisibility;
    protected $stockState;
    protected $galleryReadHandler;
    protected $helper;
    protected $stockRegistry;
    protected $scopeConfig;

    private $filterGroups;

    public $searchCriteria;
    public $filterBuilder;
    public $filterGroup;

    public function __construct(
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Api\SearchCriteriaInterface $criteria,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroup,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState,
        \Magento\Directory\Helper\Data $helper,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->productMetadata = $productMetadata;
        $this->productRepository = $productRepository;
        $this->searchCriteria = $criteria;
        $this->filterGroup = $filterGroup;
        $this->filterBuilder = $filterBuilder;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->stockState = $stockState;
        $this->helper = $helper;
        $this->stockRegistry = $stockRegistry;
        $this->scopeConfig = $scopeConfig;

        $this->searchCriteria->setPageSize(1);
        $this->searchCriteria->setCurrentPage(1);

        $this->setFilters([
            $this->filterGroup
                ->setFilters([
                    $this->filterBuilder
                        ->setField('status')
                        ->setConditionType('in')
                        ->setValue($this->productStatus->getVisibleStatusIds())
                        ->create(),
                    $this->filterBuilder
                        ->setField('visibility')
                        ->setConditionType('in')
                        ->setValue($this->productVisibility->getVisibleInSiteIds())
                        ->create(),
                ])
                ->create()
        ]);

    }

    function setFilters($filters) {
        $this->filterGroups = $filters;
    }

    function getFilters() {
        return $this->filterGroups;
    }

    function getItems() {
        try {
            $barcodeAttr = $this->scopeConfig->getValue('dataqlick/settings/barcode', 'default');

            $this->searchCriteria->setFilterGroups($this->getFilters());
            $productsList = $this->productRepository->getList($this->searchCriteria);
            $productItems = $productsList->getItems();

            $products = [];
            foreach($productItems as $productItem) {
                $raw_product = $productItem->getData();
                $product = [
                    'id' => $productItem->getId(),
                    'name' =>  $productItem->getName(),
                    'description' =>  $productItem->getDescription() ? $productItem->getDescription() : '' ,
                    'isInventory' =>  in_array($productItem->getTypeId(), ['simple']), // Inventory item or service
                    'isTaxable' =>  !empty($raw_product['tax_class_id']),
                    'isActive' =>  $productItem->getStatus() == Status::STATUS_ENABLED, //false - deleted or deactivated products
                    'qty' =>  $this->stockState->getStockQty($productItem->getId(), $productItem->getStore()->getWebsiteId()),
                    'unitPrice' =>  $productItem->getFinalPrice(1),
                    'purchaseCost' =>  isset($raw_product['price']) ? (float)$raw_product['price'] : 0,
                    'sku' =>  $productItem->getSku(),
                    'barCode' =>  $productItem->getData($barcodeAttr) ? $productItem->getData($barcodeAttr) : '',
                    'category' =>  $this->getProductCategories($productItem),
                    'imageUrl' =>  $this->getProductImages($productItem),
                    'tags' =>  '',
                    'lenght' => 0,
                    'height' => 0,
                    'width' => 0,
                    'sizeType' => 'Inch',
                    'weight' => $productItem->getWeight(),
                    'weightType' => $this->helper->getWeightUnit(),
                    'unitOfMeasure' => 'Each',
                    'updated' =>  date('c', strtotime($productItem->getUpdatedAt())), //'2017-07-24T19:09:43-04:00'
                ];
                $products[] = $product;
            }

            $totalCount = $productsList->getTotalCount();
            $page = $this->searchCriteria->getCurrentPage();
            $pagesize = $this->searchCriteria->getPageSize();
            if($pagesize < 1) {
                return $this->error('WRONG_PAGE', 'Page should be > 0');
            }
            $pagemax = ceil($totalCount / $pagesize);
            if($page > $pagemax) $page = $pagemax;
            if($page < 1) $page = 1;

            $result = [
                'count' => $totalCount,
                'page' => $page,
                'pagesize' => $pagesize,
                'pagemax' => $pagemax,
                'results' => $products,
            ];
            return $result;
        } catch (\Exception $e) {
            return $this->error('EXCEPTION', $e->getMessage());
        }
    }

    public function update($data) {
        if(empty($data['id'])) {
            throw new \Exception(__('Product id not specified'));
        }
        $product = $this->productRepository->getById($data['id']);
        if(isset($data['price'])) {
            $product->setPrice($data['price']);
            $product->save();
        }
        if(isset($data['qty'])) {
            $stockItem = $this->stockRegistry->getStockItemBySku($product->getSku());
            $stockItem->setQty($data['qty']);
            $stockItem->setIsInStock((bool)$data['qty']);
            $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
        }

        return true;


    }


    private function getProductCategories($product) {
        $version = $this->productMetadata->getVersion();
        if(version_compare($version, '2.1', 'ge')) {
            $categories = [];
            $categoryCollection = $product->getCategoryCollection()
                ->addAttributeToSelect('name');
        } else {
            $categoryIds = $product->getCategoryIds();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $categoryCollection = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory')->create();
            $categoryCollection
                ->addAttributeToSelect('name')
                ->addAttributeToFilter('entity_id', $categoryIds);
        }
        foreach($categoryCollection as $category) {
            $categories[] = $category->getName();
        }
        if(count($categories) == 0)
            return '';
        elseif(count($categories) == 1)
            return $categories[0];
        else
            return  implode(', ', $categories);
    }

    private function getProductImages($product) {
        $version = $this->productMetadata->getVersion();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if(version_compare($version, '2.1', 'ge')) {
            $galleryReadHandler = $objectManager->create('\Magento\Catalog\Model\Product\Gallery\ReadHandler');
            $galleryReadHandler->execute($product);
            $images = [];
            $imagesCollection = $product->getMediaGalleryImages();
            if(is_null($imagesCollection)) return null;
            foreach($imagesCollection as $image) {
                $images[] = $image->getUrl();
                break; // return only first image
            }
        } else {
            $store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
            $images = [$store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage()];
        }
        if(count($images) == 0)
            return '';
        elseif(count($images) == 1)
            return $images[0];
        else
            return $images;
    }

    private function error($code, $message) {
        return [
            'error' => $code,
            'message' => $message
        ];
    }

}