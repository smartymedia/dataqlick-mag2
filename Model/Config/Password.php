<?php
namespace Smartymedia\DataQlick\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Password extends \Magento\Framework\App\Config\Value {

    protected $messageManager;
    protected $dq;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Smartymedia\DataQlick\Model\DQApi $dq,
        array $data = []
    ) {
        $this->messageManager = $messageManager;
        $this->dq = $dq;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    public function afterSave() {
        $username = $this->getFieldsetDataValue('username');
        $password = $this->getValue();
        try {
            $this->dq->setCredentials($username, $password); //override credentials
            $token = $this->dq->getAuthToken(true);

            if(!$this->dq->isAuthorized()) {
                throw new \InvalidArgumentException(__('unknown problem'));
            }

        } catch(\InvalidArgumentException $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Linking error: %1', $e->getMessage()));
        }


        $this->messageManager->addSuccess( __('Successfully linked to DataQlick') );

        return parent::afterSave();
    }
}