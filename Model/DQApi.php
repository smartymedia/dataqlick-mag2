<?php
namespace Smartymedia\DataQlick\Model;

use Magento\Framework\HTTP\ZendClientFactory;

class DQApi  {
    const DQ_URL = 'https://api.dataqlick.com';
//    const DQ_URL = 'https://stage-api.dataqlick.com';
    const DQ_PROVIDER_ID = 'magento2';

    protected $logger;
    protected $config;
    protected $httpClientFactory;
    protected $scopeConfig;
    protected $cacheTypeList;
    protected $storeManager;

    protected $username, $password;

    private $token  = null;


    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        ZendClientFactory $httpClientFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Config\Model\ResourceModel\Config $configWriter,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->logger = $loggerInterface;
        $this->config = $configWriter;
        $this->httpClientFactory = $httpClientFactory;
        $this->scopeConfig = $scopeConfig;
        $this->cacheTypeList = $cacheTypeList;
        $this->storeManager = $storeManager;

        $this->username = $this->scopeConfig->getValue('dataqlick/general/username', 'default');
        $this->password = $this->scopeConfig->getValue('dataqlick/general/password', 'default');
    }

    public function setCredentials($username, $password) {
        $this->username = $username;
        $this->password = $password;
    }

    public function isAuthorized() {
        return is_array($this->token)
            && isset($this->token['shop_token'])
            && isset($this->token['customerId'])
            && isset($this->token['access_token'])
            && isset($this->token['refresh_token']);
    }

    public function getAuthToken($forceReceive = false) {
        $this->token = @unserialize($this->scopeConfig->getValue('dataqlick/general/token', 'default'));
        if(!$this->token || $forceReceive) {
            // no token, let's receive new
            $this->receiveAuthToken();
        } elseif($this->isAuthorized() && $this->token['expires'] <= time()+3600) {
            // token expired, renew
            $this->renewAuthToken();
        }
        return $this->token;
    }

    private function generateShopToken() {
        return bin2hex(openssl_random_pseudo_bytes(16));
    }

    public function getShopToken() {
        if(empty($this->token['shop_token'])) return null;
        return $this->token['shop_token'];
    }

    public function getCustomerId() {
        if(empty($this->token['customerId'])) return null;
        return $this->token['customerId'];
    }

    private function connectChannel() {
        $client = $this->httpClientFactory->create();
        $client->setUri(static::DQ_URL.'/plugin/connect');
        $client->setConfig(['timeout' => 10]);
        $client->setMethod(\Zend_Http_Client::POST);

        $shopToken = $this->generateShopToken();
        $jsonData = json_encode([
            'providerId'=> static::DQ_PROVIDER_ID, // constant
            'url'       => $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_LINK).'dataqlick/ajax',
            'token'     => $shopToken, // ApiKey generated by magento plugin to access from DQ
            'shopName'  => $this->storeManager->getStore()->getName(),
            'currency'  => $this->storeManager->getStore()->getCurrentCurrency()->getCode() // Use ISO 4217 format
        ], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        $client->setRawData($jsonData, 'application/json');
        $client->setHeaders('Authorization', $this->token['token_type'].' '.$this->token['access_token']);

        $response = $client->request();
        $result = json_decode($response->getBody(), true);
        if(isset($result['error'])) {
            throw new \InvalidArgumentException('error during shop connection - '.$result['error']);
        } elseif(isset($result['customerId'])) {
            $this->token['shop_token'] = $shopToken;
            $this->token['customerId'] = $result['customerId'];
            return $this->token;
        } else {
            throw new \InvalidArgumentException('Unexpected error');
        }
    }

    private function receiveAuthToken() {
        $this->token = null;

        if(empty($this->username) || empty($this->password)) {
            return null;
        }
        $this->token = $this->apiLogin($this->username, $this->password);

        $this->token['expires'] = time() + $this->token['expires_in'];
        $this->connectChannel();
        $this->saveAuthToken();
        return $this->token;
    }

    public function apiLogin($username, $password) {
        $jsonData = json_encode([
            'email' => $username,
            'password' => $password
        ], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);


        $client = $this->httpClientFactory->create();
        $client->setUri(static::DQ_URL.'/api/login');
        $client->setConfig(['timeout' => 10]);
        $client->setMethod(\Zend_Http_Client::POST);
        $client->setRawData($jsonData, 'application/json');

        $response = $client->request();
        $token = json_decode($response->getBody(), true);
        switch($response->getStatus()) {
            case 200: break;
            case 401:
            case 403: throw new \InvalidArgumentException('Wrong login / password');
        }

        if(empty($token['access_token'])) {
            throw new \InvalidArgumentException('Error receiving token');
        }
        return $token;
    }

    private function renewAuthToken() {
        $client = $this->httpClientFactory->create();
        $client->setUri(static::DQ_URL.'/oauth/access_token');
        $client->setConfig(['timeout' => 10]);
        $client->setMethod(\Zend_Http_Client::POST);
        $client->setParameterPost([
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->token['refresh_token'],
        ]);
        $response = $client->request();
        $customerId = $this->token['customerId'];
        $newToken = json_decode($response->getBody(), true);
        if(empty($newToken)) {
            throw new \InvalidArgumentException('Error renewing token');
        }
        $this->token = array_merge($this->token, $newToken);
        $this->token['expires'] = time() + $this->token['expires_in'];
        $this->saveAuthToken();
        return $this->token;
    }

    private function saveAuthToken() {
        $this->config->saveConfig('dataqlick/general/token', serialize($this->token), 'default', 0);
        // clear config cache
        $this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
    }

}