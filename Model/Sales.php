<?php

namespace Smartymedia\DataQlick\Model;


class Sales  {
    protected $orderRepository;
    protected $ruleRepository;
    protected $storeManager;
    protected $customers;
    protected $logger;

    private $filterGroups;

    public $filterBuilder;
    public $filterGroup;
    public $searchCriteria;

    public function __construct(
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\SalesRule\Api\RuleRepositoryInterface $ruleRepository,
        \Magento\Framework\Api\SearchCriteriaInterface $criteria,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroup,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Smartymedia\DataQlick\Model\Customers $customers,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->orderRepository = $orderRepository;
        $this->ruleRepository = $ruleRepository;
        $this->searchCriteria = $criteria;
        $this->filterGroup = $filterGroup;
        $this->filterBuilder = $filterBuilder;
        $this->storeManager = $storeManager;
        $this->customers = $customers;
        $this->logger = $logger;

        $this->searchCriteria->setPageSize(1);
        $this->searchCriteria->setCurrentPage(1);
    }

    function setFilters($filters) {
        $this->filterGroups = $filters;
    }

    function getFilters() {
        return $this->filterGroups;
    }

    function getItems() {
        try {
            $this->searchCriteria->setFilterGroups($this->getFilters());
            $ordersList = $this->orderRepository->getList($this->searchCriteria);
            $ordersItems = $ordersList->getItems();

            $storeName = $this->storeManager->getStore()->getName();

            $orders = [];
            foreach($ordersItems as $orderItem) {
                $rules = [];
                $rulesIds = $orderItem->getAppliedRuleIds();
                if($rulesIds) {
                    $rulesIds = array_unique(explode(',', $rulesIds));
                    foreach($rulesIds as $ruleId) {
                        $rule = $this->ruleRepository->getById($ruleId);
                        switch($rule->getSimpleAction()) {
                            case 'by_percent': $discountType = 'Percent'; break;
                            case 'by_fixed': $discountType = 'Sum'; break;
                            case 'cart_fixed': $discountType = 'Sum'; break;
                            default: $discountType = $rule->getSimpleAction(); break;
                        }
                        $rules[] = [
                            'id' => $rule->getRuleId(),
                            'name' => $rule->getName(),
                            'discountValue' => (float)$rule->getDiscountAmount(),
                            'discountType' => $discountType,
                            'applyToAllItems' => count($rule->getActionCondition()->getConditions()) == 0,
                        ];
                    }
                }

                $taxes = [];
                $items = [];
                $lineId = 1;
                foreach($orderItem->getAllItems() as $item) {
                    $rulesIds = $item->getAppliedRuleIds();
                    $discounts = [];
                    if($rulesIds) {
                        $rulesIds = array_unique(explode(',', $rulesIds));
                        foreach($rulesIds as $ruleId) {
                            $discounts[] = ['id' => (int)$ruleId];
                        }
                    }
                    $taxPercent = $item->getTaxPercent();
                    if($taxPercent>0) {
                        if(!isset($taxes[$taxPercent])) {
                            $taxId = count($taxes)+1;
                            $taxes[$taxPercent] = [
                                'id' => $taxId,
                                'name' => 'Magento '.$storeName.' '.$taxPercent.'% tax',
                                'rate' => (float)$taxPercent,
                                'amount' => (float)$item->getTaxAmount(),
                            ];
                        } else {
                            $taxes[(string)$taxPercent]['amount'] += (float)$item->getTaxAmount();
                            $taxId = $taxes[$taxPercent]['id'];
                        }
                    }
                    $itemsToLines[(int)$item->getProductId()] = $lineId;
                    $items[] = [
                        'lineId' => (int)$item->getItemId(),
                        'itemId' => (int)$item->getProductId(),
                        'qty' => (int)$item->getQtyOrdered(),
                        'price' => (float)$item->getPrice(),
                        'isTaxable' => $item->getProduct() ? !empty($item->getProduct()->getData('tax_class_id')) : '',
                        'discounts' => $discounts,
                        'taxes' => ($taxPercent>0 ? [['id' => $taxId]] : []),
                    ];
                    $lineId++;
                }

                $this->customers->setFilters([
                    $this->customers->filterBuilder
                        ->setField('entity_id')
                        ->setConditionType('in')
                        ->setValue([$orderItem->getCustomerId()])
                        ->create()
                ]);


                $customerResult = $this->customers->getItems();
                if(isset($customerResult['count']) && $customerResult['count'] > 0) {
                    $customer = $customerResult['results'][0];
                } else {
                    $customer = [];
                }

                $payments = [];
                $allPayments = $orderItem->getAllPayments();
                foreach($orderItem->getInvoiceCollection() as $invoice) {
                    $payments[] = [
                        'id' => (int)$invoice->getId(),
                        'type' => (isset($allPayments[0]) ? $allPayments[0]->getMethod() : '') ,
                        'amount' => (float)$invoice->getGrandTotal(),
                    ];
                }
                $refunds = [];
                foreach($orderItem->getCreditmemosCollection() as $refund) {
                    $refundItems = [];
                    foreach($refund->getItemsCollection() as $refundItem) {
                        $refundItems[] = [
                            'lineId' => (int)$refundItem->getOrderItemId(),
                            'qty' => (int)$refundItem->getQty(),
                        ];
                    }
                    $refunds[] = [
                        'id' => (int)$refund->getId(),
                        'restock' => true,
                        'items' => $refundItems,
                        'date' => date('c', strtotime($refund->getUpdatedAt())),
                    ];
                }
                $shipments = [];
                foreach($orderItem->getShipmentsCollection() as $shipment) {
                    $shipmentItems = [];
                    foreach($shipment->getItems() as $shipmentItem) {
                        $shipmentItems[] = [
                            'lineId' => (int)$shipmentItem->getOrderItemId(),
                            'qty' => (int)$shipmentItem->getOrderItem()->getQtyShipped(),
                        ];
                    }
                    $methods = $trackingNumbers = [];
                    foreach($shipment->getTracks() as $track) {
                        $methods[] = $track->getTitle();
                        $trackingNumbers[] = $track->getNumber();
                    }

/*
                    $items[] = array(
                        'lineId' => 0,
                        'itemId' => 'shipping',
                        'qty'    => 1,
                        'price'  => count($shipments) == 0 ? (float)$orderItem->getShippingAmount() : 0, // в Magento нет разбивки доставки по shippments,
                        'class'  => get_class()
                        'isTaxable' => $orderItem->getShippingTaxAmount()>0,
                        'discounts' => $shippingDiscountId ? array(array('id' => $shippingDiscountId)) : array(),
                        'taxes' => $this->makeIds($shippingTaxes),
                    );
*/

                    $shipments[] = [
                        'id' => $shipment->getId(),
                        'date' => date('c', strtotime($shipment->getUpdatedAt())),
                        'price' => count($shipments) == 0 ? (float)$orderItem->getShippingAmount() : 0, // в Magento нет разбивки доставки по shippments
                        'items' => $shipmentItems,
                        'method' => implode(', ', $methods),
                        'trackingNumber' => implode(', ', $trackingNumbers),
                    ];
                }
                $discountAmount = -round($orderItem->getGrandTotal() - $orderItem->getTaxAmount()  - $orderItem->getShippingAmount() /* - $orderItem->getDiscountAmount()  */- $orderItem->getBaseSubtotal(), 4);
                $order = [
                    'id' => (int)$orderItem->getId(),
                    'docNumber' => $orderItem->getRealOrderId(),
                    'shippingCost' => (float)$orderItem->getShippingAmount(),
                    'currency' => $orderItem->getOrderCurrency()->getCurrencyCode(),
                    'discounts' => $rules,
                    'discountAmount' => $discountAmount,
                    'taxes' => array_values($taxes),
                    'taxAmount' => (float)$orderItem->getTaxAmount(),
                    'items' => $items,
                    'customer' => $customer,
                    'payments' => $payments,
                    'refunds' => $refunds,
                    'shipments' => $shipments,
                    'updated' => date('c', strtotime($orderItem->getUpdatedAt())),
                    'date' => date('c', strtotime($orderItem->getCreatedAt())),
                    'dueDate' => date('c', strtotime($orderItem->getCreatedAt())),
                ];
                $orders[] = $order;
            }

            $totalCount = $ordersList->getTotalCount();
            $page = $this->searchCriteria->getCurrentPage();
            $pagesize = $this->searchCriteria->getPageSize();
            if($pagesize < 1) {
                return $this->error('WRONG_PAGE', 'Page should be > 0');
            }
            $pagemax = ceil($totalCount / $pagesize);
            if($page > $pagemax) $page = $pagemax;
            if($page < 1) $page = 1;

            $result = [
                'count' => $totalCount,
                'page' => $page,
                'pagesize' => $pagesize,
                'pagemax' => $pagemax,
                'results' => $orders,
            ];
            return $result;
        } catch (\Exception $e) {
            return $this->error('EXCEPTION', $e->getMessage());
        }
    }


    private function error($code, $message) {
        return [
            'error' => $code,
            'message' => $message
        ];
    }

}